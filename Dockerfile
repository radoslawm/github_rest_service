FROM tiangolo/uwsgi-nginx-flask:python3.8
ENV UWSGI_INI /app/uwsgi.ini
ENV UWSGI_CHEAPER 2
ENV UWSGI_PROCESSES 8
ENV NGINX_WORKER_PROCESSES auto
EXPOSE 5000
COPY ./app /app
WORKDIR /app
RUN pip install -r requirements.txt
