# GitHub REST API v3 / flask_RESTful #

Example solution.

## Info ##

* Example solution for GitHub REST API v3 using flask_RESTful.
* Basic test included (pytest).
* Ready for Docker.

## How does it work? ##

* Application has 3 endpoints:

1. GET '/' - returns simple Hello World message
2. POST '/token/add/<string:github_token>' - uploads GitHub token to application in order to browse repositories. Don't have token? Please see instructions [here](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token).
3. GET '/repositories/<string:repository_owner>/<string:repository_name>' - returns repository info. Keep in mind that in order to see repository info, a token must be added first.

* Token is being flushed after application restart.

## Links ##

* [crushyna / github_rest_service @ Docker Hub](https://hub.docker.com/repository/docker/crushyna/github_rest_service)
* [Running solution @ Microsoft Azure](https://github-restapi-docker.azurewebsites.net)

## Application set-up ##

* Application can be run both locally, through local Docker container and using cloud solution.
* Examples are described below.

#### Local installation ####

1. Clone from this repository
2. Open in your favourite IDE
3. In github_rest_service folder run 'pip install -r app\requirements.txt' to install all required packages.
4. In github_rest_service\app (project folder) run 'pytest' to run unit tests.
5. Run application using \github_rest_service\app\main.py (where github_rest_service\app is working directory).

#### Local Docker Container ####

* For purpose of this example, Docker Desktop for Windows 10 is used.

1. Download from [www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop) and install.
2. Run Windows command line (CMD) as administrator (recommended).
3. Type:
>     docker pull crushyna/github_rest_service:latest
4. After download is completed, run the container:
>     docker run -p 80:80 crushyna/github_rest_service:latest
5. Access application using localhost address - 127.0.0.1:80. [Postman](https://www.postman.com) application is recommended.

#### Cloud Docker Container ####

* For purpose of this example, Microsoft Azure is used.

1. Create new free account at [Microsoft Azure here](https://azure.microsoft.com/en-us/)
2. After account activation, navigate to [Azure Portal](https://portal.azure.com/#home)
3. From here, click on Resource Groups and add new one.
4. Select you subcription, name and region. Accept.
5. Within new Resource Group, add new Web App instance.
6. While creating new Web App instance, be sure to select:
	- Publish: Docker Container
	- Operating System: Linux
	- Region: same as resource group (point 4.)
	- Sku and size: Free or B1 is preffered.
	
	
7. From tab Docker, be sure to select following options:
	- Image Source: Docker Hub
	- Image and tag: crushyna/github_rest_service:latest


8. Proceed and create.
9. After the Web App instance is successfully created, proceed to it's instance.
10. In Settings > Configuration, add a new application setting:
	- Name: PORT
	- Value: 80


11. Save settings. Proceed to Overview and restart application.
12. After about 5-10 minutes, application should be available under URL shown in Overview tab.

## Contact? ##

* email: radoslawm12113@gmail.com
* github: https://github.com/crushyna