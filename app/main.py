from flask import Flask
from flask_restful import Api

from resources.add_token import AddToken
from resources.hello import HelloWorld
from resources.repository import RepositoryResource

app = Flask(__name__)
api = Api(app)
app.secret_key = b'midichlorians'

api.add_resource(HelloWorld, '/')
api.add_resource(RepositoryResource, '/repositories/<string:repository_owner>/<string:repository_name>')
api.add_resource(AddToken, '/token/add/<string:github_token>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
