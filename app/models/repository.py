import requests

from sources.config import GitHubToken, token_required


class RepositoryModel:
    """
    RepositoryModel instance is initiated from GitHub API response data.
    """

    def __init__(self, *args, **kwargs):
        for dictionary in args:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    @staticmethod
    @token_required
    def get_repository_data(repository_owner: str, repository_name: str):
        token_instance = GitHubToken()

        query_url = f"https://api.github.com/repos/{repository_owner}/{repository_name}"
        params = {
            "state": "open",
        }
        headers = {'Authorization': f'token {token_instance.token}'}
        response = requests.get(query_url, headers=headers, params=params)
        if response.status_code == 404:
            return {'message': 'User not found or requested repository does not exist!',
                    'status': 'error'}, response.status_code
        elif response.status_code == 401:
            return {'message': 'Unknown GitHub token! Please verify.',
                    'status': 'error'}, response.status_code
        else:
            repository_data = response.json()
            data_dict = {'full_name': repository_data['full_name'],
                         'description': repository_data['description'],
                         'git_clone_url': repository_data['clone_url'],
                         'number_of_stargazers': repository_data['stargazers_count'],
                         'creation_date': repository_data['created_at']}

        return data_dict, response.status_code
