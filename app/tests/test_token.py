import configparser

from sources.config import GitHubToken


def test_github_token():
    """
    Checks whether the GitHub token is available (as a system variable.
    Does NOT checks whether it is correct.
    :return:
    """
    # TODO: fix for new token solution
    config = configparser.ConfigParser()
    config.read("config.ini")
    config.set('GITHUB_TOKEN', 'token', "8e190179eb5f0e187cf608df624ac08bc621781a")

    with open('config.ini', 'w') as configfile:
        config.write(configfile)

    test_token = GitHubToken()
    assert test_token.check_token() is True
