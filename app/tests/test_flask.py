from main import app


def test_general_grevious():
    """
    A surprise to be sure,
    but a welcome one.
    :return: Kenobi's one-liner
    """
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert response.data == b'"Well hello there!"\n'


def test_repository_response():
    """
    USING flask test_client
    WHEN the selected (example) repository is requested
    THEN check if the response is valid.
    """
    response_token = app.test_client().post('/token/add/8e190179eb5f0e187cf608df624ac08bc621781a')
    response_repository = app.test_client().get('/repositories/crushyna/tensorflow_numbers_demo')

    assert response_repository.status_code == 200
    assert b'success' in response_repository.data
