from models.repository import RepositoryModel


def test_new_repository():
    """
    GIVEN specific data
    CHECK if model is created properly via dict unpacking
    """
    repo_data = {
        "full_name": "crushyna/tensorflow_numbers_demo",
        "description": "Tensorflow numbers recognition demo",
        "git_clone_url": "https://github.com/crushyna/tensorflow_numbers_demo.git",
        "number_of_stargazers": 0,
        "creation_date": "2019-11-02T10:22:42Z"
    }
    test_repo = RepositoryModel(**repo_data)

    assert test_repo.full_name == "crushyna/tensorflow_numbers_demo"
    assert test_repo.description == "Tensorflow numbers recognition demo"
    assert test_repo.git_clone_url == "https://github.com/crushyna/tensorflow_numbers_demo.git"
    assert test_repo.number_of_stargazers == 0
    assert test_repo.creation_date == "2019-11-02T10:22:42Z"
