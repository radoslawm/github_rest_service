import configparser


def token_required(f):
    def wrap(*args, **kwargs):
        if GitHubToken.check_token():
            return f(*args, **kwargs)
        else:
            return {'message': 'GitHub token not found! Please follow instructions at: '
                               'https://bitbucket.org/radoslawm/github_rest_service/src/master/ for instructions. ',
                    'status': 'error'}, 403

    wrap.__name__ = f.__name__
    return wrap


class GitHubToken:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.token = config['GITHUB_TOKEN']['token']
        self.test_token = config['GITHUB_TOKEN']['test_token']

    @staticmethod
    def check_token():
        token_instance = GitHubToken()
        if len(token_instance.token) > 10:
            return True
        else:
            return False
