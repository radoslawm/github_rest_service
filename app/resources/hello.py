from flask_restful import Resource


class HelloWorld(Resource):
    """
    Simple test endpoint.
    General Kenobi?
    """

    def get(self):
        return "Well hello there!"
