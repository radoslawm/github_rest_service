from flask_restful import Resource
from models.repository import RepositoryModel


class RepositoryResource(Resource):
    """
    Retrieve information about specific repository.
    Returns JSON response.
    """

    def get(self, repository_owner: str, repository_name: str):
        response, status_code = RepositoryModel.get_repository_data(repository_owner, repository_name)
        if not status_code == 200:
            return response, status_code
        else:
            repository = RepositoryModel(**response)
            return {'message': repository.__dict__,
                    'status': 'success'}, status_code
