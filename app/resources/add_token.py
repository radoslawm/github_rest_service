import configparser

from flask_restful import Resource
from sources.config import GitHubToken


class AddToken(Resource):
    """
    Add GitHub token to application.
    Token will be flushed after the application is restarted.
    """

    def post(self, github_token: str):
        config = configparser.ConfigParser()
        config.read("config.ini")
        config.set('GITHUB_TOKEN', 'token', github_token)

        with open('config.ini', 'w') as configfile:
            config.write(configfile)

        token_instance = GitHubToken()

        if token_instance.token == github_token:
            return {'message': f"Token added successfully!. Token: {token_instance.token}",
                    'status': 'success'}, 201

        else:
            return {'message': f'Error while adding token!, {github_token}, {token_instance.token}',
                    'status': 'error'}, 500
